class SequentialSet:
    """
    Defines a set of sequential integers
    """

    def __init__(self, start, length):
        """
        Standard initialiser
        A SequentialSet(1, 10) should contain: 1 2 3 4 5 6 7 8 9 10
        """
        self.start = start
        self.length = length if length > 0 else 0

    def numItems(self):
        """
        This tells us how many actual numbers are in the set
        """
        return self.length

    def combine(self, other):
        """
        This set will contain all the numbers it previously had as well as
        all the numbers in "other"
        """
        self.start = min(self.start, other.start)
        self.length = self.length + other.length

    def isInSequence(self, num):
        """
        Returns true if num is a member of the SequentialSet
        """

        if(num < self.start):
            return false
        else:
            return num < (self.start + self.length)

    def getAll(self):
        """
        Returns an array with every number in the set
        """
        all = []
        for num in range(self.start, self.start + self.length):
            all.append(num)
        return all
