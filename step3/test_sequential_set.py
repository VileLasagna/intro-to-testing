from sequential_set import SequentialSet

"""
We can use classes to group tests into categories
Make sure your class starts with 'Test' and each test functions
starts with `test_` so they get picked up correctly
"""

"""
In order to mark a test as failed for PyTest we need to fail an assert
Other testing systems may work slightly different, such as expecting a
return of true or false. Make sure to double check the documentation of
the system you're planning on using
"""

class TestCreation:
    """
    Tests related to whether the SequentialSet is created correctly
    """

    def test_positiveLength(self):
        """
        Some known SequentialSets and what we expect from them:

        x(1, 10) = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        y(-5, 3) = [-5, -4, -3]
        """

        # test data and known values
        start1 = 1
        length1 = 10
        results1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

        start2 = -5
        length2 = 3
        results2 = [-5, -4, -3]

        # create some sets
        set1 = SequentialSet(start1, length1)
        set2 = SequentialSet(start2, length2)

        values1 = set1.getAll()
        values2 = set2.getAll()

        # Check we match
        assert len(values1) == len(results1)
        assert len(values2) == len(results2)

        for index in range(len(results1)):
            assert results1[index] == values1[index]
        for index in range(len(results2)):
            assert results2[index] == values2[index]

    def test_nullLength(self):
        """
        A set with length 0 must have 0 items
        """

        # test data and known values
        start1 = 1
        start2 = -5


        # create some sets
        set1 = SequentialSet(start1, 0)
        set2 = SequentialSet(start2, 0)

        # check we have 0 length
        assert set1.numItems() == 0
        assert set2.numItems() == 0

        # Also double check we didn't end up with actual numbers stored
        values1 = set1.getAll()
        values2 = set2.getAll()

        assert len(values1) == 0
        assert len(values2) == 0

    def test_negativeLength(self):
            """
            A set with length <0 must have 0 items
            """

            # test data and known values
            start1 = 1
            start2 = -5


            # create some sets
            set1 = SequentialSet(start1, -1)
            set2 = SequentialSet(start2, -1)
            set3 = SequentialSet(start1, -10)
            set4 = SequentialSet(start2, -10)

            # check we have 0 length
            assert set1.numItems() == 0
            assert set2.numItems() == 0
            assert set3.numItems() == 0
            assert set4.numItems() == 0

            # Also double check we didn't end up with actual numbers stored
            values1 = set1.getAll()
            values2 = set2.getAll()
            values3 = set3.getAll()
            values4 = set4.getAll()

            assert len(values1) == 0
            assert len(values2) == 0
            assert len(values3) == 0
            assert len(values4) == 0

class TestCombine:
    """
    Tests for different combination operations of SequentialSet
    """

    """
    STEP3

    Let's write these
    """

    def test_contiguousCombine(self):
        """
        When we combine two contiguous SequentialSets, the result should have all
        of the numbers

        x(1, 5) = [1, 2, 3, 4, 5]
        y(6, 5) = [6, 7, 8, 9, 10]

        x+y = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        """

        length1 = 5
        length2 = 5
        x = SequentialSet(1, length1)
        y = SequentialSet(6, length2)

        combined = SequentialSet(1, length1)

        combined.combine(y)

        assert combined.numItems() == (x.numItems() + y.numItems())

        valuesX = x.getAll()
        valuesY = y.getAll()


        for val in valuesX:
            assert combined.isInSequence(val)
        for val in valuesY:
            assert combined.isInSequence(val)


    def test_overlappingCombine(self):
        """
        When we combine two overlapping SequentialSets, the result should have
        no repeats!

        x(1, 7) = [1, 2, 3, 4, 5, 6, 7]
        y(6, 5) = [6, 7, 8, 9, 10]

        x+y = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        """

        length1 = 7
        length2 = 5
        x = SequentialSet(1, length1)
        y = SequentialSet(6, length2)

        combined = SequentialSet(1, length1)

        combined.combine(y)

        # make sure we have the correct length
        assert combined.numItems() == 10

        valuesX = x.getAll()
        valuesY = y.getAll()

        # make sure all the required values are there
        for val in valuesX:
            assert combined.isInSequence(val)
        for val in valuesY:
            assert combined.isInSequence(val)

    def test_disjointedCombine(self):
        """
        When we combine two disjointed SequentialSets, the result will have a
        gap

        x(1, 3) = [1, 2, 3]
        y(7, 4) = [7, 8, 9, 10]

        x+y = [1, 2, 3,     7, 8, 9, 10]
        """

        length1 = 3
        length2 = 4
        x = SequentialSet(1, length1)
        y = SequentialSet(7, length2)

        combined = SequentialSet(1, length1)

        combined.combine(y)

        assert combined.numItems() == (x.numItems() + y.numItems())

        valuesX        = x.getAll()
        valuesY        = y.getAll()
        valuesCombined = combined.getAll()

        # make sure all the required values are there
        for val in valuesX:
            assert combined.isInSequence(val)
        for val in valuesY:
            assert combined.isInSequence(val)

        # make sure we don't have any extraneous values
        for val in valuesCombined:
            assert x.isInSequence(val) or y.isInSequence(val)
