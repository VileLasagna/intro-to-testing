class SequentialSet:
    """
    Defines a set of sequential integers
    """

    def __init__(self, start, length):
        """
        Standard initialiser
        A SequentialSet(1, 10) should contain: 1 2 3 4 5 6 7 8 9 10
        """

        """
        STEP2
        So we need to guard against a negative length. We could choose any number
        of ways to handle it but we've decided to just make it result in an empty
        sequence

        BONUS
        A more robust solution might also try and protect from getting arguments
        that are not integers and your test could verify that they raise the
        correct errors
        """

        self.start = start
        # We added this check here
        self.length = length if length > 0 else 0

    def numItems(self):
        """
        This tells us how many actual numbers are in the set
        """
        return self.length

    def combine(self, other):
        """
        This set will contain all the numbers it previously had as well as
        all the numbers in "other"
        """
        self.start = min(self.start, other.start)
        self.length = self.length + other.length

    def isInSequence(self, num):
        """
        Returns true if num is a member of the SequentialSet
        """

        if(num < self.start):
            return false
        else:
            return num < (self.start + self.length)

    def getAll(self):
        """
        Returns an array with every number in the set
        """
        all = []
        for num in range(self.start, self.start + self.length):
            all.append(num)
        return all
