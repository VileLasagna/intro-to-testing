class SequentialSet:
    """
    Defines a set of sequential integers
    """

    """
    STEP 4

    It turns out that combining special cases made our class much more complicated
    than we initially thought

    Thanks to testing we discovered we may need to manage several ranges
    """

    def __init__(self, start, length):
        """
        Standard initialiser
        A SequentialSet(1, 10) should contain: 1 2 3 4 5 6 7 8 9 10
        """
        if(length > 0):
            self.ranges = [ {'begin':start, 'range': length} ]
        else:
            self.ranges = []

    def numItems(self):
        """
        This tells us how many actual numbers are in the set
        """

        total = 0
        for index in self.ranges:
            total += index['range']
        return total

    def combine(self, other):
        """
        This set will contain all the numbers it previously had as well as
        all the numbers in "other"
        """
        # add all ranges from other
        self.ranges.extend(other.ranges)

        # no need to do anything if we only have one range
        if len(self.ranges) < 2:
            return

        # sort our ranges from their starting points
        self.ranges.sort(key = lambda range: range['begin'])

        allSorted = False

        while not allSorted:
            if len(self.ranges) < 2:
                break
            for num in range(0, len(self.ranges) - 1):
                currentBegin = self.ranges[num]['begin']
                currentRange = self.ranges[num]['range']
                currentEnd   = currentBegin + currentRange

                nextBegin = self.ranges[num+1]['begin']
                nextRange = self.ranges[num+1]['range']
                nextEnd   = nextBegin + nextRange

                #check for contiguous
                if(nextBegin == (currentEnd + 1)):
                    # if contiguous, add lengths and get rid of extraneous range
                    self.ranges[num]['range'] += nextRange
                    self.ranges.pop(num+1)
                    continue

                #check for overlapping
                if(nextBegin <= currentEnd):
                    self.ranges[num]['range'] += ( nextRange - (currentBegin + currentRange - nextBegin))
                    self.ranges.pop(num+1)
                    continue

            allSorted = True



    def isInSequence(self, num):
        """
        Returns true if num is a member of the SequentialSet
        """
        # check each individual range
        for index in self.ranges:
            if (index['begin'] <= num) and ( ( index['begin'] + index['range'] ) > num):
                return True
        return False

    def getAll(self):
        """
        Returns an array with every number in the set
        """
        all = []
        for index in self.ranges:
            for num in range(index['begin'], index['begin'] + index['range']):
                all.append(num)
        return all
